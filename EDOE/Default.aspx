﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EDOE.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EDOE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>



    <%--    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1801417446820349',
                cookie: true,
                xfbml: true,
                version: 'v2.12'
            });

            FB.AppEvents.logPageView();

        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });


    </script>--%>


    <script src="https://connect.facebook.net/en_US/all.js" type="text/javascript"></script>

    <script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <%--<script src="scripts/all.js" type="text/javascript"></script> --%>

    <script>

        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            ref.parentNode.insertBefore(js, ref);
        }(document));


        $("document").ready(function () {
            // Initialize the SDK upon load
            FB.init({
                appId: '1801417446820349', // App ID
                channelUrl: '//' + window.location.hostname + '/channel', // Path to your 
                // Channel File
                scope: 'id,name,gender,user_birthday,email', // This to get the user details back 
                // from Facebook
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });
            // listen for and handle auth.statusChange events
            FB.Event.subscribe('auth.statusChange', OnLogin);
        });

        function OnLogin(response) {
            if (response.authResponse) {
                FB.api('/me?fields=id,name,gender,email,birthday', LoadValues);
            }
        }


        //This method will load the values to the labels
        function LoadValues(me) {
            if (me.name) {
                document.getElementById('displayname').innerHTML = me.name;
                document.getElementById('FBId').innerHTML = me.id;
                document.getElementById('DisplayEmail').innerHTML = me.email;
                document.getElementById('Gender').innerHTML = me.gender;
                document.getElementById('DOB').innerHTML = me.birthday;
                document.getElementById('auth-loggedin').style.display = 'block';
            }
        }


    </script>

    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">
                    <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">How edoe works  </a></li>
                    <li><a href="#">Business </a></li>
                    <li><a href="#">Security </a></li>
                    <li><a href="ContactUs.aspx">Contact Us </a></li>
                    <li><a href="SignIn.aspx">Sign In </a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <form id="form1" runat="server">
        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section id="sendmoney">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="sendmoney">
                            <h1>Send money and</h1>
                            <h1>make purchases at</h1>
                            <h1>approved merchants </h1>
                            <!-- <a class="btn btn-primary btnshadow padfbtn" href="#" role="button">Sign Up with Facebook OnClick="Login" </a>-->
                            <%-- <asp:Button ID="btnLoginWithFacebook" CssClass="btn btn-primary btnshadow padfbtn" runat="server" Text="Sign Up with Facebook" />--%>

                            <a class="btn btn-primary btnshadow padfbtn mart40" href="SignUp.aspx" role="button">Sign Up with Facebook </a>
                            <!-- Details -->
                            <%--        <div id="auth-status">
            <div id="auth-loggedin" style="display: none">
                Hi, <span id="displayname"></span>
                <br />
                Your Facebook ID : <span id="FBId"></span>
                <br />
                Your Email : <span id="DisplayEmail"></span>
                <br />
                Your Sex:, <span id="Gender"></span>
                <br />
                Your Date of Birth :, <span id="DOB"></span>
                <br />
            </div>
        </div>--%>
                            <p class="emailadd">or with your <a href="SignUp.aspx">email address </a></p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="mobscreen">
                            <img src="images/phone.png" class="img-responsive" alt="Mobile image" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->
    </form>
    <!----- Footer ---------->
    <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <!----- End Footer ------->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/navstick.js"></script>
</body>
</html>
