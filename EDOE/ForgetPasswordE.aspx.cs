﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace EDOE
{
    public partial class ForgetPasswordE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            string username = string.Empty;
            string FullName = string.Empty;
            string EncodeEmail = string.Empty;

            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {

                string userFirstName = txtUserEmail.Text.ToString().Trim();
                SqlCommand cmd;

                try
                {

                    cmd = new SqlCommand("select email_id, full_name FROM User_Registration WHERE email_id= @Email");
                    cmd.Connection = con;
                    //int obj = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Parameters.AddWithValue("@Email", userFirstName);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            username = sdr["email_id"].ToString();
                            FullName = sdr["full_name"].ToString();

                            EncodeEmail = Convert.ToBase64String(Encoding.Unicode.GetBytes(username)); ;

                            //Encoding.Unicode.GetString(Convert.FromBase64String(username));

                        }
                    }

                    if (!string.IsNullOrEmpty(username))
                    {


                        string to = userFirstName; //To address    
                        string from = "raurnet.vinod@gmail.com"; //From address    
                        MailMessage message = new MailMessage(from, to);

                        string mailbody = string.Format("Hi {0}, <br> Click <a href='http://localhost:1384/ResetPassword.aspx?EmailId={1}'>here</a> to reset your password. <br> Thanks,<br>EDOE", FullName, EncodeEmail);
                        message.Subject = "Reset your password";
                        message.Body = mailbody;
                        message.BodyEncoding = Encoding.UTF8;
                        message.IsBodyHtml = true;
                        SmtpClient client = new SmtpClient("smtp.gmail.com", 25); //Gmail smtp    
                        System.Net.NetworkCredential basicCredential1 = new
                        System.Net.NetworkCredential("raurnet.pramendra@gmail.com", "pramendra@2018");
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        //smtpclient.EnableSsl = true;
                        client.Credentials = basicCredential1;
                        try
                        {
                            client.Send(message);
                        }

                        catch (Exception ex)
                        {
                            throw ex;
                        }


                        lblError.ForeColor = System.Drawing.Color.Green;
                        lblError.Text = "Password reset link has been send to your mail, Please check your email.";
                        lblError.Visible = true;
                        txtUserEmail.Text = "";
                        txtUserEmail.Focus();
                    }

                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();

                    con.Dispose();
                }
            }
        }
    }
}