﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardSetting.aspx.cs" Inherits="EDOE.DashboardSetting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EODE </title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="profile"><span>
                            <%-- <img src="images/profile.jpg" class="img-responsive" alt="Responsive image" />--%>
                            <a href="Profile.aspx">
                                <asp:Image ID="UserProfileDp" CssClass="img-responsive" alt="Responsive image" runat="server" />
                            </a>
                        </span></li>
                        <li><a href="Profile.aspx">
                            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></a></li>
                        <li><a href="Setting.aspx">Settings </a></li>
                        <li><a href="#">Statement </a></li>
                        <li><a href="ContactSupport.aspx">Contact Support </a></li>
                        <li><a href="LogOut.aspx">Log out </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section class="chatbgcolor">
            <div class="topchatbor">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 padb10">
                            <h2 class="webcolor">
                                <a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i>Back</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-lg-offset-1">
                        <div class="right-menu">
                            <div class="padb30">
                                <h1>Setting </h1>
                            </div>
                            <ul>
                                <li><a href="#">Profile </a></li>
                                <li><a href="#">Banks & Cards </a></li>
                                <li><a href="#">Privacy </a></li>
                                <li><a href="#">Blocked Users </a></li>
                                <li><a href="#">Legal Info </a></li>
                                <li><a href="#">Send Feedback </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7  bgwhite chatborder">
                        <div class="signin-box padtl30 vhfull">
                            <div class="right-menu">
                                <ul>
                                    <li><a href="#">User Agreement </a></li>
                                    <li><a href="#">Privacy Policy </a></li>
                                    <li><a href="#">Licenses </a></li>
                                    <li><a href="#">Payment Methods Rights </a></li>
                                    <li><a href="#">EDOE Cardholder Agreements </a></li>
                                    <li><a href="#">EDOE Card Privacy Policy </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->
    </form>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/navstick.js"></script>
</body>
</html>
