﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="EDOE.ContactUs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EDOE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">
                    <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">How edoe works  </a></li>
                    <li><a href="#">Business </a></li>
                    <li><a href="#">Security </a></li>
                    <li class="active"><a href="ContactUs.aspx">Contact Us </a></li>
                    <li><a href="SignIn.aspx">Sign In </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <form id="form1" runat="server">
        <!----- Start Text Area -------------->
        <section class="chatbgcolor">
            <div class="container">
                <div class="row">
                    <div class="topspace"></div>
                    <div class="col-lg-8 col-lg-offset-2">
                        
                        <div class="signin-box mart30">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                            <div class="form-horizontal">
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        
                                        <label for="" class="col-sm-12 padb10">NAME </label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtContactName" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtContactName" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Style="position: absolute; left: 15px;" runat="server" ErrorMessage="INVALID NAME" ControlToValidate="txtContactName" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="^[A-Z][a-z]*(\s[A-Z][a-z]*)+$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="" class="col-sm-12 padt20">PHONE NUMBER </label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtContactNumber" CssClass="form-control ftextstyle" runat="server" MaxLength="11"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="REQUIRED" SetFocusOnError="true" ControlToValidate="txtContactNumber" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Style="position: absolute; left: 15px;" runat="server" ErrorMessage="INVALID PHONE" ControlToValidate="txtContactNumber" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>

                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="" class="col-sm-12 padt20">EMAIL </label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtContactEmail" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtContactEmail" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Style="position: absolute; left: 15px;" runat="server" ErrorMessage="INVALID EMAIL" ControlToValidate="txtContactEmail" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="" class="col-sm-12 padt20">SUBJECT </label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtContactSubject" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtContactSubject" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="" class="col-sm-12 padt20">DESCRIPTION </label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtContactDesc" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>




                                            <asp:Button ID="btnContactSubmit" CssClass="btn btn-primary text-center mart20 pull-right" runat="server" Text="Submit" OnClick="btnContactSubmit_Click" />


                                        </div>

                                    </div>
                                    

                                </div>
                            </div>
                            <!-- End form  -->
                        </div>
                    </div>

                </div>

                <div class="topspace"></div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->
    </form>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/navstick.js"></script>
</body>
</html>
