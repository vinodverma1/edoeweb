﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace EDOE
{
    public partial class SignIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;


            using (SqlConnection con = new SqlConnection(CS))
            {
                string userEmail = txtUserEmail.Text.ToString().Trim();
                string userPassword = txtUserPass.Text.ToString().Trim();

                int userStatus = 0;
                SqlCommand cmd;
                Regex regex = new Regex(@"^.+\@.+\..+$");

                Regex regexM=new Regex(@"[0-9]+");

                try
                {

                    Match match = regex.Match(userEmail);

                    Match matchM=regexM.Match(userEmail);

                    con.Open();
                    if (match.Success)
                    {
                        cmd = new SqlCommand("select COUNT(email_id) FROM User_Registration WHERE email_id='" + userEmail + "' AND password='" + userPassword + "'");

                        cmd.Connection = con;
                        int obj = Convert.ToInt32(cmd.ExecuteScalar());
                        if (obj > 0)
                        {
                            Session["Email"] = userEmail;
                            Response.Redirect("Profile.aspx");
                            //Session.Remove("Email");
                        }

                        else if (obj == 0)
                        {
                            lblError.Text = "Someting wrong, Please try again!";
                            lblError.Visible = true;
                            txtUserEmail.Focus();
                        }



                    }

                    else if (matchM.Success)
                    {
                        cmd = new SqlCommand("select COUNT(*) FROM User_Registration WHERE contact_no='" + userEmail + "' AND password='" + userPassword + "'");

                        cmd.Connection = con;
                        int obj = Convert.ToInt32(cmd.ExecuteScalar());
                        if (obj > 0)
                        {
                            Session["Email"] = userEmail;
                            Response.Redirect("Profile.aspx");
                        }

                        else if (obj == 0)
                        {
                            lblError.Text = "Someting wrong, Please try again!";
                            lblError.Visible = true;
                            txtUserEmail.Focus();
                        }
                    }


                    else
                    {
                        cmd = new SqlCommand("select COUNT(*) FROM User_Registration WHERE UserName='" + userEmail + "' AND password='" + userPassword + "'");

                        cmd.Connection = con;
                        int obj = Convert.ToInt32(cmd.ExecuteScalar());
                        if (obj > 0)
                        {
                            Session["Email"] = userEmail;
                            Response.Redirect("Profile.aspx");
                        }

                        else if (obj == 0)
                        {
                            lblError.Text = "Someting wrong, Please try again!";
                            lblError.Visible = true;
                            txtUserEmail.Focus();
                        }
                    }



                    //con.Open();
                    //cmd = new SqlCommand("select COUNT(*) FROM User_Registration WHERE email_id='" + userEmail + "' AND password='" + userPassword + "'");
                    //cmd.Connection = con;
                    //int obj = Convert.ToInt32(cmd.ExecuteScalar());
                    //if (obj > 0)
                    //{
                    //    Session["Email"] = userEmail;
                    //    Response.Redirect("DashboardSetting.aspx");
                    //}

                    //else if (obj == 0)
                    //{
                    //    lblError.Text = "Someting wrong, Please try again!";
                    //    lblError.Visible = true;
                    //    txtUserEmail.Focus();
                    //}





                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();

                    con.Dispose();
                }
            }
        }
    }
}