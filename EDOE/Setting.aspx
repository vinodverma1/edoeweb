﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Setting.aspx.cs" Inherits="EDOE.Setting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EODE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default new-navstyle">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" />
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="profile"><span>
                            <%-- <img src="images/profile.jpg" class="img-responsive" alt="Responsive image" />--%>
                            <a href="Profile.aspx">
                                <asp:Image ID="UserProfileDp" CssClass="img-responsive" alt="Responsive image" runat="server" />
                            </a>
                        </span></li>
                        <li><a href="Profile.aspx">
                            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></a></li>
                        <li><a href="Setting.aspx">Settings </a></li>
                        <li><a href="#">Statement </a></li>
                        <li><a href="ContactSupport.aspx">Contact Support </a></li>
                        <li><a href="LogOut.aspx">Log out </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section class="chatbgcolor">
            <div class="container">
                <div class="row">
                    <div class="topspace"></div>
                    <div class="col-lg-11 col-lg-offset-1">
                        <div class="padb30">
                            <h1>Setting </h1>
                        </div>
                    </div>
                    <div class="col-lg-3 col-lg-offset-1">
                        <div class="right-menu tabnavstyle">
                            <ul class="nav nav-stacked">
                                <li><a href="Profile.aspx">Profile </a></li>
                                <li class="active"><a href="#tab_b" data-toggle="pill">Banks & Cards </a></li>
                                <li><a href="#tab_c" data-toggle="pill">Privacy </a></li>
                                <li><a href="#tab_d" data-toggle="pill">Blocked Users</a></li>
                                <li><a href="#tab_e" data-toggle="pill">Notifications</a></li>
                                <li><a href="#tab_f" data-toggle="pill">Legal Information</a></li>
                                <li><a href="#tab_g" data-toggle="pill">Send Feedback</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="signin-box mart30">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_b">
                                    <div class="bankcard-box">
                                        <div class="borderbottom">
                                            <div class="row">
                                                <h4>BANK ACCOUNTS</h4>
                                                <div class="col-md-11 bankaccount">
                                                 <%--   <img src="images/bank.png" class="img-responsive" alt="bank" />
                                                    <h5>Bank of EU, Personal Checking ....1234</h5>
                                                    <p>In use -  payments, bank transfers </p>--%>



                                                    <asp:Repeater ID="RepForBankAccount" runat="server">

                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 10%; padding: 0; margin: 0;">
                                                                        <%--<asp:Label ID="lblComment" runat="server" Text='<%#Eval("card_type") %>' />--%>
                                                                        <img src="images/bank.png" class="img-responsive" alt="bank" />
                                                                    </td>
                                                                    <td style="width: 80%; padding-left: 20px; margin-top: 5px; padding-top: 10px;">
                                                                        <asp:Label ID="lblUser" runat="server" Text='<%# Eval("bank_name")+", Personal Checking .... " %>' />

                                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("AccountNumber") %>' />
                                                                        <p>In use -  purchases </p>

                                                                    </td>

                                                                    <td style="width:10%; ">
                                                                        <div style="float:right">
                                                                            <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <%--<tr>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>--%>
                                                            </table>
                                                        </ItemTemplate>

                                                    </asp:Repeater>





                                                    <a href="AddAccount.aspx">Link Bank Account </a>














                                                </div>
                                                <%--<div class="col-md-1">
                                                    <div class="rightarrow">
                                                        <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>--%>

                                            </div>
                                        </div>
                                        <div class="borderbottom">
                                            <div class="row">
                                                <h4>CARDS</h4>
                                                <div class="col-md-11 bankaccount">
                                                    <%--<img src="images/bank.png" class="img-responsive" alt="bank" />--%>
                                                    <%--<h5>Bank of EU, Personal Checking ....1234</h5>
                                                    <p>In use -  purchases </p>--%>

                                                    <asp:Repeater ID="RepDetails" runat="server">

                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 10%; padding: 0; margin: 0;">
                                                                        <%--<asp:Label ID="lblComment" runat="server" Text='<%#Eval("card_type") %>' />--%>
                                                                        <img src="images/bank.png" class="img-responsive" alt="bank" />
                                                                    </td>
                                                                    <td style="width: 80%; padding-left: 20px; margin-top: 5px; padding-top: 10px;">
                                                                        <asp:Label ID="lblUser" runat="server" Text='<%# Eval("full_name")+", Personal Checking .... " %>' />

                                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CardNumber") %>' />
                                                                        <p>In use -  purchases </p>

                                                                    </td>

                                                                    <td style="width:10%; ">
                                                                        <div style="float:right">
                                                                            <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <%--<tr>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>--%>
                                                            </table>
                                                        </ItemTemplate>

                                                    </asp:Repeater>








                                                    <a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">Add Debit or Credit Card </a>

                                                    <!----- Start  Modal -------->
                                                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                                        <div class="modal-dialog modal-md" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    <h4 class="modal-title" id="mySmallModalLabel">
                                                                        <h2>Add Debit or Credit Card </h2>
                                                                    </h4>
                                                                </div>
                                                                <div class="fullboxpop">
                                                                    <!-------start form ------->
                                                                    <div class="add_card">
                                                                        <div class="row">
                                                                            <div class="form-group">
                                                                                <label for="" class="col-sm-12 mart10">CARD NUMBER </label>
                                                                                <div class="col-lg-12">
                                                                                    <asp:TextBox ID="txtCardNumber" CssClass="form-control ftextstyle" runat="server" placeholder="Card Number"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="CardValidate" runat="server" ValidationGroup="AddCardValidation" ControlToValidate="txtCardNumber" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="AddCardValidation" Style="position: absolute; left: 15px;" ControlToValidate="txtCardNumber" runat="server" ErrorMessage="Only Numbers allowed" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="" class="col-sm-12 mart30">EXPIRATION DATE </label>
                                                                                <div class="col-lg-12">
                                                                                    <asp:TextBox ID="txtExpDate" runat="server" CssClass="form-control ftextstyle" placeholder="MM/YY"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtExpDate" ValidationGroup="AddCardValidation" runat="server" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="AddCardValidation" Style="position: absolute; left: 15px;" ValidationExpression="^(?:0[1-9]|1[0-2])\/[0-9]{2}$" ControlToValidate="txtExpDate" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" runat="server" ErrorMessage="INVALID"></asp:RegularExpressionValidator>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="" class="col-sm-12 mart30">SECURITY CODE </label>
                                                                                <div class="col-lg-12">
                                                                                    <asp:TextBox ID="txtSecurityCode" CssClass="form-control ftextstyle" runat="server" placeholder="CVV"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtSecurityCode" ValidationGroup="AddCardValidation" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" runat="server" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="AddCardValidation" Style="position: absolute; left: 15px;" ControlToValidate="txtSecurityCode" SetFocusOnError="true" ValidationExpression="^([0-9]{3,4})$" ForeColor="Red" Font-Bold="true" ErrorMessage="INVALID"></asp:RegularExpressionValidator>


                                                                                </div>

                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="" class="col-sm-12 mart30">ZIP CODE </label>
                                                                                <div class="col-lg-12">
                                                                                    <asp:TextBox ID="txtZipCode" CssClass="form-control ftextstyle" runat="server" placeholder="Zip Code"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="INVALID" ValidationGroup="AddCardValidation" ControlToValidate="txtZipCode" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12 mart40">
                                                                                    <button type="submit" class="btn btn-primary" onclick="history.back();">Cancel </button>

                                                                                    <asp:Button ID="btnSubmitCard" CssClass="btn btn-primary pull-right" ValidationGroup="AddCardValidation" runat="server" Text="Add Card" OnClick="AddCadrInfoToDB" />

                                                                                    <%--<button type="submit" class="btn btn-primary pull-right">Add Card </button>--%>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-------End form ------->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!----- End of Modal -------->
                                                </div>
                                               <%-- <div class="col-md-1">
                                                    <div class="rightarrow">
                                                        <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="marb20"></div>
                                </div>
                                <asp:Label ID="lblError" runat="server"></asp:Label>

                                <div class="tab-pane" id="tab_c">
                                    <h4>Pane C</h4>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames
											ac turpis egestas.
                                    </p>
                                </div>
                                <div class="tab-pane" id="tab_d">
                                    <h4>Pane D</h4>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames
											ac turpis egestas.
                                    </p>
                                </div>
                                <div class="tab-pane" id="tab_e">
                                    <h4>Pane E</h4>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames
											ac turpis egestas.
                                    </p>
                                </div>
                                <div class="tab-pane" id="tab_f">
                                    <h4>Pane F</h4>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames
											ac turpis egestas.
                                    </p>
                                </div>
                                <div class="tab-pane" id="tab_g">
                                    <h4>Pane G</h4>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames
											ac turpis egestas.
                                    </p>
                                </div>
                            </div>
                            <!-- tab content -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->

    </form>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/navstick.js"></script>
</body>
</html>
