﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace EDOE
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }


            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {


                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select * from User_Registration  where email_id='" + Session["Email"] + "' OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();
                        string fullName = ds.Tables[0].Rows[0]["full_name"].ToString();

                        var names = fullName.Split(' ');
                        string firstName = names[0];
                        string lastName = names[1];

                        txtFirstName.Text = firstName;
                        txtLastName.Text = lastName;
                        txtUserName.Text = ds.Tables[0].Rows[0]["UserName"].ToString();

                        txtEmailID.Text = ds.Tables[0].Rows[0]["email_id"].ToString();
                        txtPhoneNumber.Text = ds.Tables[0].Rows[0]["contact_no"].ToString();
                        txtSiteUrl.Text = "http://www.edoe.com/" + ds.Tables[0].Rows[0]["UserName"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                            imgProfileSet.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                            imgProfileSet.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }




        }

        protected void btnSubmittoProfile_Click(object sender, EventArgs e)
        {
            int rowsAffected = 0;

            string Fullname = txtFirstName.Text + " " + txtLastName.Text;
            string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;

            string query = "UPDATE User_Registration SET contact_no = @ContactNo, full_name=@FullName, UserName = @UserName WHERE email_id = @UserEmailId";
            if (txtChangePassword.Text.Trim() != "")
            {
                query = "UPDATE User_Registration SET password = @NewPassword ,contact_no = @ContactNo, full_name=@FullName, UserName = @UserName WHERE email_id = @UserEmailId";
            }

            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@ContactNo", txtPhoneNumber.Text);
                        cmd.Parameters.AddWithValue("@FullName", Fullname);
                        cmd.Parameters.AddWithValue("@UserName", txtUserName.Text);
                        cmd.Parameters.AddWithValue("@UserEmailId", Session["Email"]);
                        if (txtChangePassword.Text.Trim() != "")
                        {
                            cmd.Parameters.AddWithValue("@NewPassword", txtChangePassword.Text.Trim());

                        }
                        cmd.Connection = con;
                        con.Open();
                        rowsAffected = cmd.ExecuteNonQuery();
                        con.Close();




                    }
                }
                if (rowsAffected > 0)
                {
                    lblError.Visible = true;
                    lblError.ForeColor = Color.Green;
                    lblError.Text = "Profile has been changed successfully.";

                    // Response.Redirect("Profile.aspx");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.ForeColor = Color.Red;
                    lblError.Text = "Something went wrong, Please try again.";
                }
            }

            // }











        }

        protected void btnSaveProfilePic(object sender, EventArgs e)
        {
            string uploadFileName = "";
            string uploadFilePath = "";
            if (flUserProfilePic.HasFile)
            {
                string ext = Path.GetExtension(flUserProfilePic.FileName).ToLower();
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                {
                    uploadFileName = Guid.NewGuid().ToString() + ext;
                    uploadFilePath = Path.Combine(Server.MapPath("~/ProfilePic/"), uploadFileName);
                    try
                    {
                        flUserProfilePic.SaveAs(uploadFilePath);
                        //impPrev.ImageUrl = "~/ProfilePic/" + uploadFileName;
                        //panCrop.Visible = true;




                        int rowsAffected = 0;
                        string query = "UPDATE User_Registration SET profile_img = @NewProfilePicTo WHERE email_id = @Username OR contact_no=@ContactNO";
                        string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(constr))
                        {
                            using (SqlCommand cmd = new SqlCommand(query))
                            {
                                using (SqlDataAdapter sda = new SqlDataAdapter())
                                {
                                    cmd.Parameters.AddWithValue("@Username", Session["Email"]);
                                    cmd.Parameters.AddWithValue("@ContactNO", Session["Email"]);
                                    cmd.Parameters.AddWithValue("@NewProfilePicTo", uploadFileName);
                                    cmd.Connection = con;
                                    con.Open();
                                    rowsAffected = cmd.ExecuteNonQuery();
                                    con.Close();
                                }
                            }
                            if (rowsAffected > 0)
                            {
                                UserProfileDp.ImageUrl = "~/ProfilePic/" + uploadFileName;
                                imgProfileSet.ImageUrl = "~/ProfilePic/" + uploadFileName;
                                lblError.Visible = true;
                                lblError.ForeColor = Color.Green;
                                lblError.Text = "Profile picture has been changed successfully.";
                                // Response.Redirect("EditUserProfile.aspx");
                            }
                            else
                            {
                                lblError.Visible = true;
                                lblError.ForeColor = Color.Red;
                                lblError.Text = "Something went wrong, Please try again..";
                            }
                        }


                        //////////////////End



                    }
                    catch (Exception)
                    {
                        lblError.Text = "Error! Please try again.";
                    }
                }
                else
                {
                    lblError.Text = "Selected file type not allowed!";
                }
            }
            else
            {
                lblError.Text = "Please select file first!";
            }

        }





    }
}