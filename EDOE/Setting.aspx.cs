﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EDOE
{
    public partial class Setting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }




            if(!IsPostBack)
            {
                BindRepeaterData();
                BindRepeaterDataAccount();
            }


           






            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {

                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select full_name, profile_img from User_Registration  where email_id='" + Session["Email"] + "' OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }






        }




        protected void BindRepeaterData()
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();

                //string JoinQuery = "SELECT User_Registration.id, User_Card_details.Upd_id, RIGHT(User_Card_details.card_number, 4) as CardNumber, User_Card_details.card_type, User_Card_details.exp_date, User_Card_details.cvv_number, User_Card_details.zip_code, Bank_Details.bd_id, Bank_Details.bank_name FROM User_Registration JOIN User_Card_details ON User_Registration.id = User_Card_details.Upd_id JOIN Bank_Details ON User_Card_details.Bank_Details_bd_id = Bank_Details.bd_id WHERE User_Registration.email_id = '" + Session["Email"] + "'";

                string JoinQuery = "SELECT User_Registration.id, User_Registration.full_name, User_Card_details.Upd_id, RIGHT(User_Card_details.card_number, 4) as CardNumber, User_Card_details.card_type, User_Card_details.exp_date, User_Card_details.cvv_number, User_Card_details.zip_code FROM User_Registration JOIN User_Card_details ON User_Registration.id = User_Card_details.Upd_id WHERE User_Registration.email_id = '" + Session["Email"] + "'";



                SqlCommand cmd = new SqlCommand(JoinQuery, con);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                RepDetails.DataSource = ds;
                RepDetails.DataBind();
                con.Close();
            }
        }



        protected void BindRepeaterDataAccount()
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();

                string JoinQuery = "SELECT RIGHT(Ua_AccountNumber, 4) as AccountNumber, User_id, bank_name FROM User_Account_details2 JOIN Bank_Details ON User_Account_details2.Ua_bankId = Bank_Details.bd_id WHERE User_Account_details2.User_id = '" + Session["Email"] + "'";


                SqlCommand cmd = new SqlCommand(JoinQuery, con);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                RepForBankAccount.DataSource = ds;
                RepForBankAccount.DataBind();
                con.Close();
            }
        }
















        protected void AddCadrInfoToDB(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;


            using (SqlConnection con = new SqlConnection(CS))
            {

                string CardNumber = txtCardNumber.Text.ToString();
                string ExpDate = txtExpDate.Text.ToString();
                string SecurityCode = txtSecurityCode.Text.ToString();
                string ZipCode = txtZipCode.Text.ToString();


                //int userStatus = 0;
                string CreditCardAdddate = DateTime.Now.ToString("dd-MM-yyyy");
                SqlCommand cmd;

                try
                {

                    con.Open();
                    cmd = new SqlCommand("select COUNT(*) FROM User_Card_details WHERE card_number='" + CardNumber + "'");
                    cmd.Connection = con;
                    int obj = Convert.ToInt32(cmd.ExecuteScalar());
                    if (obj > 0)
                    {
                        lblError.Visible = true;
                        lblError.ForeColor = System.Drawing.Color.Red;
                        lblError.Text = "This card number already exist, please try again!";
                        txtCardNumber.Focus();
                    }

                    else if (obj == 0)
                    {

                        using (cmd = new SqlCommand("SELECT id FROM User_Registration WHERE email_id=@Email", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@Email", Session["Email"]);
                            //con.Open();
                            object Uid = cmd.ExecuteScalar();
                            if (Uid != null)
                            {
                                int UserId = Convert.ToInt32(Uid);


                                string QueryStr = "INSERT INTO User_Card_details  (card_number, exp_date, cvv_number, zip_code, User_Registration_id, card_added_date) VALUES('" + CardNumber + "',  '" + ExpDate + "', '" + SecurityCode + "',  '" + ZipCode + "', " + UserId + ", " + CreditCardAdddate + ")";

                                cmd = new SqlCommand(QueryStr, con);
                                cmd.ExecuteNonQuery();

                                lblError.Visible = true;
                                lblError.ForeColor = System.Drawing.Color.Green;
                                lblError.Text = "Your card successfully added.";

                            }
                            con.Close();
                        }






                        // Response.Redirect("SignIn.aspx");
                    }
                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();

                    con.Dispose();
                }
            }

        }


    }
}