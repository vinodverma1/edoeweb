﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EDOE
{
    public partial class ContactSupport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }


            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {

                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select full_name, profile_img from User_Registration  where email_id='" + Session["Email"] + "' OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }




        }

        protected void btnContactSubmit_Click(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;


            using (SqlConnection con = new SqlConnection(CS))
            {

                string ContactName = txtContactName.Text.ToString().Trim();
                string ContactNumber = txtContactNumber.Text.ToString().Trim();
                string ContactEmail = txtContactEmail.Text.ToString().Trim();
                string ContactSubject = txtContactSubject.Text.ToString().Trim();
                string ContactDesc = txtContactDesc.Text.ToString().Trim();

                string ContactSubmitDate = DateTime.Today.ToString("dd-MM-yyyy");
                SqlCommand cmd;

                try
                {
                    string QueryStr = "INSERT INTO tblContactSupport  (Cs_ContactName, Cs_ContactNumber, Cs_ContactEmail, Cs_ContactSubject,  Cs_ContactDesc, Cs_SubmitBy, Cs_Status, Cs_ContactDate) VALUES('" + ContactName + "',  '" + ContactNumber + "', '" + ContactEmail + "', '" + ContactSubject + "', '" + ContactDesc + "', '" + Session["Email"] + "', 'Open', " + ContactSubmitDate + ")";

                    con.Open();
                    cmd = new SqlCommand(QueryStr, con);
                    cmd.ExecuteNonQuery();


                    txtContactName.Text = "";
                    txtContactNumber.Text = "";
                    txtContactEmail.Text = "";
                    txtContactSubject.Text = "";
                    txtContactDesc.Text = "";

                    lblError.ForeColor = System.Drawing.Color.Green;
                    lblError.Text = "Your query has been submited. Thank you!";

                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }



    }
}