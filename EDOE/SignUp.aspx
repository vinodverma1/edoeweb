﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="EDOE.SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EODE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default blankmenu bor-bot4">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="../">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
                </div>

            </div>
            <!-- /.container-fluid -->
        </nav>
        <!----- Start Text Area -------------->
        <section class="padtb50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="signin-box">

                            <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="Red"></asp:Label>

                            <h1 class="text-center">Create Your Account </h1>
                            <div class="fb-btn">
                                <%--<button type="submit" class="btn btn-primary text-center signup-fb ">Sign Up with Facebook </button>--%>
                                <asp:Button ID="btnSignUpWithFB" ValidationGroup="NoValidation" CssClass="btn btn-primary text-center signup-fb btnshadow" runat="server" Text="Sign Up with Facebook" />

                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext" id="lblFN">FIRST NAME</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtUserFirstName" CssClass="fdesign form-control" runat="server" onfocus="lblChangeColorFn()" onblur="lblChangeColorBlurFn()"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SimpleSignUp" ControlToValidate="txtUserFirstName" ForeColor="Red" Font-Bold="true" runat="server" ErrorMessage="REQUIRED" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" style="position:absolute; left:15px;" runat="server" ErrorMessage="INVALID NAME" ControlToValidate="txtUserFirstName" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="^[A-Z][a-zA-Z]*$"></asp:RegularExpressionValidator>
                                            
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext" id="lblLN">LAST NAME</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtUserLastName" CssClass="fdesign form-control" runat="server" onfocus="lblChangeColorLn()" onblur="lblChangeColorBlurLn()"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SimpleSignUp" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserLastName" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" style="position:absolute; left:15px;" runat="server" ErrorMessage="INVALID LAST NAME" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="^[A-Z][a-zA-Z]*$" ControlToValidate="txtUserLastName"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-12 padb10 einputtext" id="lblEmail">EMAIL </label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtUserEmail" CssClass="form-control" runat="server" TextMode="Email" onfocus="lblChangeColorEl()" onblur="lblChangeColorBlurEl()"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SimpleSignUp" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserEmail" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" style="position:absolute; left:15px;" ValidationGroup="SimpleSignUp" runat="server" ControlToValidate="txtUserEmail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ErrorMessage="INVALID EMAIL."></asp:RegularExpressionValidator>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-12 padb10 einputtext" id="lblPN">PHONE </label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtUserPhone" CssClass="form-control" TextMode="Phone" runat="server" onfocus="lblChangeColorEl()" MaxLength="11"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SimpleSignUp" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserPhone" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" style="position:absolute; left:15px;" runat="server" ValidationGroup="SimpleSignUp" ControlToValidate="txtUserPhone" ErrorMessage="INVALID PHONE"  ValidationExpression="[0-9]+" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RegularExpressionValidator>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-12 padb10 einputtext" id="lblPS">PASSWORD</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtUserPassword" CssClass="form-control" TextMode="Password" runat="server" onfocus="lblChangeColorPs()" onblur="lblChangeColorBlurPs()"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SimpleSignUp" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserPassword" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group marb90 mart10">
                                    <div class="col-sm-1 mart30">
                                        <asp:RadioButton ID="rbnUserAgree" runat="server" />
                                    </div>
                                    <div class="col-sm-11">
                                        <p class="fontsstyle">
                                            I agree to the Consent to Receive Electronic Disclosures and understand that we'll send account notices to the email address you provided.
                                        </p>
                                    </div>
                                    <div class="col-sm-1 mart15">
                                        <asp:RadioButton ID="rbnUserRead" runat="server" />
                                    </div>
                                    <div class="col-sm-11">
                                        <p class="fontsstyle">I have read and agree to edoe's User Agreement and Privacy Policy </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 font16 mart10"><a href="javascript:void(0);">Helpful Information </a></div>
                                    <div class="col-sm-6">
                                        <asp:Button ID="btnUserSignUp" ValidationGroup="SimpleSignUp" CssClass="btn btn-primary pull-right" runat="server" Text="Sign Up" OnClick="btnUserSignUp_Click" />

                                    </div>
                                </div>
                                <p class="fontsstyle font18 text-center mart20">By Submitting, you confirm that you are authorized to use the number entered and agree to receive SMS texts to verify you own the number. Carrier fees may apply.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->
        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->

        <script>

            function lblChangeColorFn() {
                document.getElementById("lblFN").style.color = "#0b78e3";
            }
            function lblChangeColorBlurFn() {
                document.getElementById("lblFN").style.color = "#666666";
            }

            function lblChangeColorLn() {
                document.getElementById("lblLN").style.color = "#0b78e3";
            }
            function lblChangeColorBlurLn() {
                document.getElementById("lblLN").style.color = "#666666";
            }
            function lblChangeColorEl() {
                document.getElementById("lblEmail").style.color = "#0b78e3";
            }
            function lblChangeColorBlurEl() {
                document.getElementById("lblEmail").style.color = "#666666";
            }
            function lblChangeColorPn() {
                document.getElementById("lblPN").style.color = "#0b78e3";
            }
            //function lblChangeColorBlurPn() {
            //    document.getElementById("lblPN").style.color = "#666666";
            //}

            function lblChangeColorPs() {
                document.getElementById("lblPS").style.color = "#0b78e3";
            }
            function lblChangeColorBlurPs() {
                document.getElementById("lblPS").style.color = "#666666";
            }

        </script>




      <%--  <script> onblur="textBoxOnBlur(this);"
            function textBoxOnBlur(elementRef) {
                var elementValue = elementRef.value;

                // Remove all "(", ")", "-", and spaces...
                //elementValue = elementValue.replace(/\(/g, '');
                //elementValue = elementValue.replace(/\)/g, '');
                //elementValue = elementValue.replace(/\-/g, '');
                //elementValue = elementValue.replace(/\s+/g, '')

                //if ( elementValue.length < 10 )
                //{
                //   // alert('The phone number needs 10 characters');
                //   // elementRef.select();
                //    //elementRef.focus();

                //    return;
                //}
                //else
                if (elementValue.length == 10) {
                    document.getElementById("lblPN").style.color = "#666666";
                    elementRef.value = (elementValue.substr(0, 5) + ' ' + elementValue.substr(5, 5));
                }
            }

        </script>--%>


        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slider.js"></script>
        <script src="js/navstick.js"></script>
    </form>
</body>
</html>
