﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUserProfile.aspx.cs" Inherits="EDOE.EditUserProfile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EODE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default new-navstyle">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" />
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="profile"><span>
                            <%-- <img src="images/profile.jpg" class="img-responsive" alt="Responsive image" />--%>
                            <a href="Profile.aspx">
                                <asp:Image ID="UserProfileDp" CssClass="img-responsive" alt="Responsive image" runat="server" />
                            </a>
                        </span></li>
                        <li class="active"><a href="Profile.aspx">
                            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></a></li>
                        <li><a href="Profile.aspx">Settings </a></li>
                        <li><a href="#">Statement </a></li>
                        <li><a href="ContactSupport.aspx">Contact Support </a></li>
                        <li><a href="LogOut.aspx">Log out </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section class="chatbgcolor">
            <div class="container">
                <div class="row">

                    <div class="topspace"></div>

                    <div class="col-lg-3 col-lg-offset-1">
                        <div class="right-menu tabnavstyle">
                            <ul class="nav nav-stacked">
                                <li class="active"><a href="#upload_pic" data-toggle="pill">Upload Profile Pic </a></li>
                                <li><a href="#change_name" data-toggle="pill">Change User Name </a></li>
                                <li><a href="#change_pass" data-toggle="pill">Change Password </a></li>
                                <li><a href="#change_mob" data-toggle="pill">Change Number </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        <div class="signin-box mart30">
                            <div class="tab-content">
                                <div class="tab-pane active" id="upload_pic">
                                    <div class="form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><strong>Upload files</strong> <small></small></div>
                                                    <div class="panel-body">
                                                        <div class="input-group image-preview">
                                                            <input placeholder="" type="text" class="form-control image-preview-filename hei34" disabled="disabled" />
                                                            <!-- don't give a name === doesn't send on POST/GET -->
                                                            <span class="input-group-btn">
                                                                <!-- image-preview-clear button -->
                                                                <%--<button type="button" class="btn btn-default image-preview-clear" style="display: none;"><span class="glyphicon glyphicon-remove"></span>Clear </button>--%>
                                                                <!-- image-preview-input -->
                                                                <div class="btn btn-default image-preview-input">
                                                                    <span class="glyphicon glyphicon-folder-open"></span><span class="image-preview-input-title">Browse</span>
                                                                    <asp:FileUpload ID="flUserProfilePic" accept="image/png, image/jpeg, image/gif" runat="server" onchange="ShowPreview(this)" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" SetFocusOnError="false" ControlToValidate="flUserProfilePic" ForeColor="Red" Font-Bold="true" runat="server" ValidationGroup="DPUploadValid" ErrorMessage="Required"></asp:RequiredFieldValidator>

                                                                    <!-- rename it -->
                                                                </div>
                                                            </span>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <span class="input-group-btn padt20 uploadimg">
                                                                    <asp:Image ID="impPrev" runat="server" />


                                                                </span>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="input-group-btn pull-right">
                                                                    <asp:Button ID="btnUserDPUpload" CssClass="btn btn-primary text-center mart20 pull-right" ValidationGroup="DPUploadValid" OnClick="btnSaveProfilePic" runat="server" Text="Upload" />
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <!-- /input-group image-preview [TO HERE]-->
                                                        <%--<br />
                                                        <!-- Drop Zone -->
                                                        <div class="upload-drop-zone" id="drop-zone">Or drag and drop files here </div>--%>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="change_name">
                                    <div class="form-horizontal">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="" class="col-sm-12 padb10">CHANGE USER NAME </label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtUserNameChange" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="REQUIRED" ValidationGroup="UserNameValid" ControlToValidate="txtUserNameChange" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                                    <asp:Button ID="btnUpdateUserName" CssClass="btn btn-primary text-center mart20 pull-right" ValidationGroup="UserNameValid" runat="server" Text="Submit" OnClick="UpdateToDbUserName" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="change_pass">
                                    <div class="form-horizontal">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="" class="col-sm-12 padb10">CHANGE PASSWORD </label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtUserPass" CssClass="form-control ftextstyle" TextMode="Password" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserPass" ValidationGroup="ChangePassValid" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="" class="col-sm-12 padt20">CONFIRM PASSWORD </label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtConfNewPass" CssClass="form-control ftextstyle" TextMode="Password" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtConfNewPass" ValidationGroup="ChangePassValid" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Missed match new password and confirm new password" ValidationGroup="ChangePassValid" ControlToCompare="txtUserPass" SetFocusOnError="true" ControlToValidate="txtConfNewPass" ForeColor="Red" Font-Bold="true" Style="position: absolute; left: 16px;"></asp:CompareValidator>
                                                    <asp:Button ID="btnChangePassword" runat="server" CssClass="btn btn-primary text-center mart20 pull-right" ValidationGroup="ChangePassValid" Text="Submit" OnClick="ChangeUserPass" />
                                                </div>
                                                <asp:Label ID="lblErrorForChangePass" runat="server"></asp:Label>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="change_mob">
                                    <div class="form-horizontal">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="" class="col-sm-12 padb10">CHANGE PHONE NUMBER </label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtChangePhone" CssClass="form-control ftextstyle" runat="server" MaxLength="11"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtChangePhone" ValidationGroup="ChangePhoneValid" runat="server" ErrorMessage="REQUIRED" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="INVALID PHONE" ControlToValidate="txtChangePhone" ValidationGroup="ChangePhoneValid" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>


                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="" class="col-sm-12 padt20">CONFIRM PHONE NUMBER </label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtChangeConfirmPhone" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtChangeConfirmPhone" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" runat="server" ErrorMessage="REQUIRED" ValidationGroup="ChangePhoneValid"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Confirm phone number dose't match!" ValidationGroup="ChangePhoneValid" ControlToCompare="txtChangePhone" SetFocusOnError="true" ControlToValidate="txtChangeConfirmPhone" ForeColor="Red" Font-Bold="true" Style="position: absolute; left: 16px;"></asp:CompareValidator>
                                                    <asp:Button ID="btnChangePhone" ValidationGroup="ChangePhoneValid" CssClass="btn btn-primary text-center mart20 pull-right" runat="server" Text="Submit" OnClick="SubmitChangePhoneClick" />
                                                </div>
                                                <asp:Label ID="lblErrorForChangePhon" runat="server"></asp:Label>



                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- tab content -->
                        </div>
                    </div>

                </div>
                <div class="topspace"></div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </form>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/navstick.js"></script>

    <script type="text/javascript">
        function ShowPreview(input) {
            if (input.files && input.files[0]) {
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    $('#impPrev').attr('src', e.target.result);
                    $('#impPrev').height(200);
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>
</html>
