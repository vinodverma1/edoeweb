﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


namespace EDOE
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUserSignUp_Click(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;


            using (SqlConnection con = new SqlConnection(CS))
            {

                string userFirstName = txtUserFirstName.Text.ToString().Trim();
                string userLastName = txtUserLastName.Text.ToString().Trim();

                string userFullName = userFirstName + " " + userLastName;

                string Username = userFirstName + "-" + userLastName;

                string userEmail = txtUserEmail.Text.ToString().Trim();
                string userPhone = txtUserPhone.Text.ToString().Trim();
                string userPassword = txtUserPassword.Text.ToString().Trim();

                int userStatus = 0;
                DateTime userRegisDate = DateTime.Now;
                SqlCommand cmd;

                try
                {


                    //const string accountSid = "AC282e2f1e43418f6c27c492fac0a99e0d";
                    //const string authToken = "c149b7c18ae88c075a3351c500b3a072";
                    //TwilioClient.Init(accountSid, authToken);

                    //var to = new PhoneNumber("+919990025748");
                    //var message = MessageResource.Create(
                    //    to,
                    //    from: new PhoneNumber("+14156507361"),
                    //    body: "This is the ship that made the Kessel Run in fourteen parsecs?");

                    //Console.WriteLine(message.Sid);





                    con.Open();
                    cmd = new SqlCommand("select COUNT(*)FROM User_Registration WHERE email_id='" + userEmail + "'");
                    cmd.Connection = con;
                    int obj = Convert.ToInt32(cmd.ExecuteScalar());
                    if (obj > 0)
                    {
                        lblError.Text = "This emial id already exist, please try again!";
                        lblError.Visible = true;
                        txtUserEmail.Focus();
                    }

                    else if (obj == 0)
                    {
                        //string QueryStr = "INSERT INTO User_Registration  (email_id, contact_no, password, full_name,  status, registration_date) VALUES('" + userEmail + "',  '" + userPhone + "', '" + userPassword + "',  '" + userFullName + "', '" + userStatus + "', '" + userRegisDate + "')";

                        string QueryStr = "INSERT INTO User_Registration  (email_id, contact_no, password, full_name, status, UserName) VALUES('" + userEmail + "',  '" + userPhone + "', '" + userPassword + "',  '" + userFullName + "', '" + userStatus + "', '" + Username + "')";

                        cmd = new SqlCommand(QueryStr, con);

                        cmd.ExecuteNonQuery();
                        Response.Redirect("SignIn.aspx");


                    }
                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();

                    con.Dispose();
                }
            }
        }
    }
}