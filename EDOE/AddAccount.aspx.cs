﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EDOE
{
    public partial class AddAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }


            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {

                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select full_name, profile_img from User_Registration  where email_id='" + Session["Email"] + "' OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            
            
            
            
            
            
            
            if (!this.IsPostBack)
            {
                string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT bd_id, bank_name FROM Bank_Details ORDER BY bank_name ASC"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        con.Open();
                        ddlBankName.DataSource = cmd.ExecuteReader();
                        ddlBankName.DataTextField = "bank_name";
                        ddlBankName.DataValueField = "bd_id";
                        ddlBankName.DataBind();
                        con.Close();
                    }
                }
                ddlBankName.Items.Insert(0, new ListItem("--Select Bank Name--", "0"));
            }
        }

        protected void btnAddAccountSubmit_Click(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;


            using (SqlConnection con = new SqlConnection(CS))
            {

                string AccountHolderName = txtAccountHolderName.Text.ToString().Trim();
                int bankId = Convert.ToInt32(ddlBankName.SelectedValue);



                string AccountName = txtAccountName.Text.ToString().Trim();
                string BankAccountNumber = txtBankAccountNumber.Text.ToString().Trim();
                //string AccountName = txtAccountName.Text.ToString().Trim();

                int userStatus = 0;
                string userRegisDate = DateTime.Today.ToString("dd-MM-yyyy");
                SqlCommand cmd;

                try
                {
                    con.Open();
                    cmd = new SqlCommand("select COUNT(*) FROM User_Account_details2 WHERE Ua_AccountNumber='" + BankAccountNumber + "'");
                    cmd.Connection = con;
                    int obj = Convert.ToInt32(cmd.ExecuteScalar());
                    if (obj > 0)
                    {
                        lblError.Text = "This account already exist, please try again!";
                        lblError.Visible = true;
                        txtBankAccountNumber.Focus();
                    }

                    else if (obj == 0)
                    {
                        string QueryStr = "INSERT INTO User_Account_details2  (Ua_accountHolderName, Ua_AccountNumber, Ua_AccountIFSC, Ua_bankId,  Ua_bankAddress, Ua_UserAmount, Ua_AddedDate, Ua_AccontUpdate, User_id) VALUES('" + AccountHolderName + "',  '" + BankAccountNumber + "', '', '" + bankId + "', '', '" + AccountName + "', " + userRegisDate + ", '', '" + Session["Email"] + "')";

                        cmd = new SqlCommand(QueryStr, con);

                        cmd.ExecuteNonQuery();


                        txtAccountHolderName.Text = "";
                        txtAccountName.Text = "";
                        txtBankAccountNumber.Text = "";



                        lblError.Visible = true;
                        lblError.ForeColor = System.Drawing.Color.Green;
                        lblError.Text = "Account successfully added.";  


                        //Response.Redirect("SignIn.aspx");
                    }
                }

                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }

        }
    }
}