﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="EDOE.ForgetPassword" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EODE</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default blankmenu bor-bot4">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="../">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="signin-box">
                            <h1 style="text-align: center;">Forget Password</h1>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-12 padb10">EMAIL</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtUserEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserEmail" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-12 padb10">NEW PASSWORD</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtUserPass" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtUserPass" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-12 padb10">CONFIRM NEW PASSWORD</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtConfNewPass" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="REQUIRED" ControlToValidate="txtConfNewPass" ForeColor="Red" Font-Bold="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Missed match new password and confirm new password" ControlToCompare="txtUserPass" SetFocusOnError="true" ControlToValidate="txtConfNewPass" ForeColor="Red" Font-Bold="true" Style="position: absolute; left: 16px;"></asp:CompareValidator>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6"><a href="SignIn.aspx">Sign In</a></div>
                                    <div class="col-sm-6">
                                        <asp:Button ID="btnSignIn" CssClass="btn btn-primary pull-right" runat="server" Text="Reset Password" OnClick="btnSignIn_Click" />
                                    </div>
                                </div>
                                <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                            </div>
                            <p class="text-center">New? <a href="SignUp.aspx">Sign Up!</a></p>




                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slider.js"></script>
        <script src="js/navstick.js"></script>
    </form>
</body>
</html>
