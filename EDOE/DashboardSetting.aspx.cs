﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EDOE
{
    public partial class DashboardSetting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }

            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {

                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select full_name, profile_img from User_Registration  where email_id='" + Session["Email"] + "' OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }



        }
    }
}