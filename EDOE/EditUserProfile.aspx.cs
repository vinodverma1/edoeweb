﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace EDOE
{
    public partial class EditUserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("~/SignIn.aspx");
            }



            if (!IsPostBack)
            {
                string CS = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    try
                    {

                        SqlCommand cmd;
                        string str;
                        con.Open();

                        str = "select full_name, profile_img from User_Registration  where email_id='" + Session["Email"] + "'  OR contact_no='" + Session["Email"] + "' OR UserName='" + Session["Email"] + "'";
                        cmd = new SqlCommand(str, con);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ltrUserName.Text = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string ProfilePic = ds.Tables[0].Rows[0]["profile_img"].ToString();

                        if (ProfilePic != "")
                        {
                            UserProfileDp.ImageUrl = "~/ProfilePic/" + ProfilePic;
                        }

                        else if (ProfilePic == "")
                        {
                            UserProfileDp.ImageUrl = "~/images/profile.jpg";
                        }

                        con.Close();
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }




        }

        protected void ChangeUserPass(object sender, EventArgs e)
        {
            int rowsAffected = 0;
            string query = "UPDATE User_Registration SET password = @NewPassword WHERE email_id = @Username OR contact_no=@ContactNO";
            string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@Username", Session["Email"]);
                        cmd.Parameters.AddWithValue("@ContactNO", Session["Email"]);
                        cmd.Parameters.AddWithValue("@NewPassword", txtUserPass.Text);
                        cmd.Connection = con;
                        con.Open();
                        rowsAffected = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (rowsAffected > 0)
                {
                    txtUserPass.Text = "";
                    txtConfNewPass.Text = "";
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Green;
                    lblMsg.Text = "Password has been changed successfully.";
                }
                else
                {
                    txtUserPass.Text = "";
                    txtConfNewPass.Text = "";
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = "Email does not match with our database records.";
                }
            }

        }


        //////////////// Chane phone number

        protected void SubmitChangePhoneClick(object sender, EventArgs e)
        {
            int rowsAffected = 0;
            string query = "UPDATE User_Registration SET contact_no = @NewPhone WHERE email_id = @Username OR contact_no=@ContactNO";
            string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@Username", Session["Email"]);
                        cmd.Parameters.AddWithValue("@ContactNO", Session["Email"]);
                        cmd.Parameters.AddWithValue("@NewPhone", txtChangePhone.Text);
                        cmd.Connection = con;
                        con.Open();
                        rowsAffected = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (rowsAffected > 0)
                {
                    txtChangePhone.Text = "";
                    txtChangeConfirmPhone.Text = "";

                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Green;
                    lblMsg.Text = "Phone number has been changed successfully.";
                }
                else
                {
                    txtChangePhone.Text = "";
                    txtChangeConfirmPhone.Text = "";

                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = "Something went wrong, Please try again..";
                }
            }
        }


        ////////////////// Save profile pic 


        protected void btnSaveProfilePic(object sender, EventArgs e)
        {
            string uploadFileName = "";
            string uploadFilePath = "";
            if (flUserProfilePic.HasFile)
            {
                string ext = Path.GetExtension(flUserProfilePic.FileName).ToLower();
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                {
                    uploadFileName = Guid.NewGuid().ToString() + ext;
                    uploadFilePath = Path.Combine(Server.MapPath("~/ProfilePic/"), uploadFileName);
                    try
                    {
                        flUserProfilePic.SaveAs(uploadFilePath);
                        //impPrev.ImageUrl = "~/ProfilePic/" + uploadFileName;
                        //panCrop.Visible = true;




                        int rowsAffected = 0;
                        string query = "UPDATE User_Registration SET profile_img = @NewProfilePicTo WHERE email_id = @Username OR contact_no=@ContactNO";
                        string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(constr))
                        {
                            using (SqlCommand cmd = new SqlCommand(query))
                            {
                                using (SqlDataAdapter sda = new SqlDataAdapter())
                                {
                                    cmd.Parameters.AddWithValue("@Username", Session["Email"]);
                                    cmd.Parameters.AddWithValue("@ContactNO", Session["Email"]);
                                    cmd.Parameters.AddWithValue("@NewProfilePicTo", uploadFileName);
                                    cmd.Connection = con;
                                    con.Open();
                                    rowsAffected = cmd.ExecuteNonQuery();
                                    con.Close();
                                }
                            }
                            if (rowsAffected > 0)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.Green;
                                lblMsg.Text = "Profile has been changed successfully.";
                               // Response.Redirect("EditUserProfile.aspx");
                            }
                            else
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.Red;
                                lblMsg.Text = "Something went wrong, Please try again..";
                            }
                        }


                        //////////////////End



                    }
                    catch (Exception)
                    {
                        lblMsg.Text = "Error! Please try again.";
                    }
                }
                else
                {
                    lblMsg.Text = "Selected file type not allowed!";
                }
            }
            else
            {
                lblMsg.Text = "Please select file first!";
            }

        }



        //////////////////////// User Name 


        protected void UpdateToDbUserName(object sender, EventArgs e)
        {
            int rowsAffected = 0;
            string query = "UPDATE User_Registration SET UserName = @UserNameToDB WHERE email_id = @Username OR contact_no=@ContactNO";
            string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@Username", Session["Email"]);
                        cmd.Parameters.AddWithValue("@ContactNO", Session["Email"]);
                        cmd.Parameters.AddWithValue("@UserNameToDB", txtUserNameChange.Text);
                        cmd.Connection = con;
                        con.Open();
                        rowsAffected = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (rowsAffected > 0)
                {
                  
                    txtUserNameChange.Text = "";

                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Green;
                    lblMsg.Text = "User name has been changed successfully.";
                }
                else
                {
                    txtUserNameChange.Text = "";


                    lblMsg.Visible = true;
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = "Something went wrong, Please try again..";
                }
            }
        }









    }
}