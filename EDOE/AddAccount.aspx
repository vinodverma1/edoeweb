﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAccount.aspx.cs" Inherits="EDOE.AddAccount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EDOE </title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default new-navstyle">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" />
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="profile"><span>
                            <%-- <img src="images/profile.jpg" class="img-responsive" alt="Responsive image" />--%>
                            <a href="Profile.aspx">
                                <asp:Image ID="UserProfileDp" CssClass="img-responsive" alt="Responsive image" runat="server" />
                            </a>
                        </span></li>
                        <li><a href="Profile.aspx">
                            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></a></li>
                        <li class="active"><a href="Profile.aspx">Settings </a></li>
                        <li><a href="#">Statement </a></li>
                        <li><a href="ContactSupport.aspx">Contact Support </a></li>
                        <li><a href="LogOut.aspx">Log out </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!----- Start Text Area -------------->
        <section class="chatbgcolor">
            <div class="topchatbor">
                <div class="container">
                    <div class="row">
                        <div class="topspace"></div>
                        <div class="col-lg-11 col-lg-offset-1">
                            <div class="padb30">
                                <h1>Setting </h1>
                            </div>
                        </div>
                        <div class="col-lg-3 col-lg-offset-1">
                            <div class="right-menu tabnavstyle">
                                <ul class="nav nav-stacked">
                                    <li><a href="Profile.aspx">Profile </a></li>
                                    <li class="active"><a href="Setting.aspx">Banks & Cards </a></li>
                                    <li><a href="#tab_c" data-toggle="pill">Privacy </a></li>
                                    <li><a href="#tab_d" data-toggle="pill">Blocked Users</a></li>
                                    <li><a href="#tab_e" data-toggle="pill">Notifications</a></li>
                                    <li><a href="#tab_f" data-toggle="pill">Legal Information</a></li>
                                    <li><a href="#tab_g" data-toggle="pill">Send Feedback</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7  bgwhite chatborder">
                            <div class="signin-box mart30">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label" style="margin-top: 12px;">Account Holder Name </label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtAccountHolderName" runat="server" CssClass="form-control" placeholder="Put your Name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAccountHolderName" ForeColor="Red" SetFocusOnError="true" Font-Bold="true" runat="server" ValidationGroup="AddAccountInfo" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label" style="margin-top: 12px;">Bank Name</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlBankName" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlBankName" InitialValue="0" SetFocusOnError="true" Font-Bold="true" ForeColor="Red" runat="server" ValidationGroup="AddAccountInfo" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label" style="margin-top: 12px;">Account Name </label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtAccountName" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtAccountName" ForeColor="Red" SetFocusOnError="true" Font-Bold="true" runat="server" ValidationGroup="AddAccountInfo" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label" style="margin-top: 12px;">Bank Account Number </label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtBankAccountNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtBankAccountNumber" ForeColor="Red" Font-Bold="true" SetFocusOnError="true" runat="server" ValidationGroup="AddAccountInfo" ErrorMessage="REQUIRED"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <%--<div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Routing Number</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtRautingNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>--%>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <asp:Button ID="btnAddAccountSubmit" ValidationGroup="AddAccountInfo" CssClass="btn btn-default" runat="server" Text="Save" OnClick="btnAddAccountSubmit_Click" />
                                        <%--<asp:Button ID="btnAddAccountCancel" CssClass="btn btn-default" runat="server" Text="Cancel" />--%>
                                        <a class="btn btn-default" href="#" role="button" onclick="history.back();">Cancel</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lblError" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slider.js"></script>
        <script src="js/navstick.js"></script>
    </form>
</body>
</html>
