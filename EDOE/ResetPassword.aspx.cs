﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;

namespace EDOE
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            int rowsAffected = 0;
            string EmailId = string.Empty;

            if (string.IsNullOrEmpty(Request.QueryString["EmailId"]))
            {
                Response.Redirect("SignIn.aspx");
            }

            EmailId = Encoding.Unicode.GetString(Convert.FromBase64String(Request.QueryString["EmailId"]));


            string query = "UPDATE User_Registration SET password = @NewPassword WHERE email_id = @Username";
            string constr = ConfigurationManager.ConnectionStrings["ConnStringDb"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@Username", EmailId);
                        cmd.Parameters.AddWithValue("@NewPassword", txtUserPass.Text);
                        cmd.Connection = con;
                        con.Open();
                        rowsAffected = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (rowsAffected > 0)
                {
                    lblError.Visible = true;
                    lblError.ForeColor = Color.Green;
                    lblError.Text = "Password has been changed successfully.";
                }
                else
                {
                    lblError.Visible = true;
                    lblError.ForeColor = Color.Red;
                    lblError.Text = "Email does not match with our database records.";
                }
            }




        }
    }
}