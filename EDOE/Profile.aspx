﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="EDOE.Profile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>EDOE </title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default new-navstyle">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../">
                        <img src="images/logo.png" class="img-responsive" alt="Logo" />
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="profile"><span>
                            <%-- <img src="images/profile.jpg" class="img-responsive" alt="Responsive image" />--%>
                            <a href="Profile.aspx">
                                <asp:Image ID="UserProfileDp" CssClass="img-responsive" alt="Responsive image" runat="server" />
                            </a>
                        </span></li>
                        <li><a href="Profile.aspx">
                            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></a></li>
                        <li class="active"><a href="Profile.aspx">Settings </a></li>
                        <li><a href="#">Statement </a></li>
                        <li><a href="ContactSupport.aspx">Contact Support </a></li>
                        <li><a href="LogOut.aspx">Log out </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- End Top Header -->
        <!----- Start Text Area -------------->
        <section class="padtb50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-lg-offset-1 padb30 padl20">
                        <h1>Settings </h1>
                    </div>
                    <div class="col-lg-3 col-lg-offset-1">
                        <div class="right-menu tabnavstyle">
                            <ul class="nav nav-stacked">
                                <li class="active"><a href="Profile.aspx">Profile </a></li>
                                <li><a href="Setting.aspx">Banks & Cards </a></li>
                                <li><a href="#">Privacy </a></li>
                                <li><a href="#">Blocked Users </a></li>
                                <li><a href="#">Notifications</a></li>
                                <li><a href="#">Legal Information </a></li>
                                <li><a href="#">Send Feedback </a></li>
                            </ul>
                        </div>
                    </div>
                    <asp:Label ID="lblError" runat="server" style="margin-left:15px;"></asp:Label>
                    <div class="col-lg-7">
                        <div class="signin-box">
                            <div class="form-horizontal">

                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-lg-12">
                                            
                                            <div class="profilepic col-sm-8">
                                                <h6 class="text-center profilepic">PROFILE PHOTO </h6>
                                                <asp:Image ID="imgProfileSet" CssClass="img-responsive img-circle" alt="profile pic" runat="server" />

                                            </div>
                                            <div class="btn btn-default image-preview-input col-sm-4 mart40 pull-right">
                                                <span class="glyphicon glyphicon-folder-open"></span><span class="image-preview-input-title">Change Picture</span>
                                                <asp:FileUpload ID="flUserProfilePic" accept="image/png, image/jpeg, image/gif" runat="server" onchange="ShowPreview(this)" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" SetFocusOnError="false" ControlToValidate="flUserProfilePic" ForeColor="Red" Font-Bold="true" runat="server" ValidationGroup="DPUploadValid" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                                <asp:Button ID="btnCancelUDp" CssClass="btn btn-primary text-center mart20 pull-right" runat="server" Text="Cancel" Style="display: None; padding:6px 12px; font-size:14px; font-weight:300;" OnClientClick="CanclebTNcLICK()" />

                                                <asp:Button ID="btnUserDPUpload" CssClass="btn btn-primary text-center mart20 pull-right" ValidationGroup="DPUploadValid" OnClick="btnSaveProfilePic" runat="server" Text="Save" Style="display: None; padding:6px 12px; font-size:14px; font-weight:300;  margin-right:10px;" />


                                                <!-- rename it -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">FIRST NAME</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtFirstName" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName" ErrorMessage="REQUIRED" SetFocusOnError="true" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">LAST NAME</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtLastName" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="REQUIRED" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">USERNAME</label>
                                            <div class="col-sm-12">
                                                <asp:Label ID="lblUserAt" runat="server" Text="@"></asp:Label>
                                                <asp:TextBox ID="txtUserName" CssClass="form-control ftextstyle padl20" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 padb30"></label>
                                            <div class="col-sm-12">
                                                <%--<asp:TextBox ID="txtSiteUrl1" CssClass="form-control ftextstyle" Text="http://www.edoe.com" Enabled="false" runat="server"></asp:TextBox>--%>
                                                <asp:Label ID="txtSiteUrl" runat="server" CssClass="form-control ftextstyle"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">EMAIL ADDRESS </label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtEmailID" CssClass="form-control ftextstyle" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">PHONE NUMBER </label>
                                            <div class="col-sm-12 mart10">
                                                <asp:TextBox ID="txtPhoneNumber" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                                <%-- <asp:Label ID="txtPhoneNumber" runat="server"></asp:Label> <a href="EditUserProfile.aspx"> Edit</a>--%>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="REQUIRED" SetFocusOnError="true" ForeColor="Red" Font-Bold="true" ControlToValidate="txtPhoneNumber"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">CHANGE PASSWORD </label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtChangePassword" TextMode="Password" CssClass="form-control ftextstyle" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="col-sm-12 padb10 einputtext">CHANGE CONFIRM PASSWORD </label>
                                            <div class="col-sm-12 mart10">
                                                <asp:TextBox ID="txtChangeConPass" CssClass="form-control ftextstyle" TextMode="Password" runat="server"></asp:TextBox>


                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="PASSWORD NOT MATCH" ControlToCompare="txtChangePassword" SetFocusOnError="true" ControlToValidate="txtChangeConPass" ForeColor="Red" Font-Bold="true"></asp:CompareValidator>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <%--<div class="col-lg-12">
                                            <label for="" class="col-sm-12 padb10 einputtext">CHANGE PASSWORD </label>
                                            <div class="col-sm-12 mart10">
                                                <%--<asp:TextBox ID="txtPassWord" TextMode="Password" CssClass="form-control ftextstyle mart10" runat="server" ></asp:TextBox>--%>
                                        <%--  <a href="EditUserProfile.aspx">Change Password</a>
                                            </div>
                                        </div>--%>
                                        <div class="col-lg-12">
                                            <label for="" class="col-sm-12 padb10 einputtext">PIN </label>
                                            <div class="col-sm-12 mart10">
                                                <%--<asp:TextBox ID="txtPassWord" TextMode="Password" CssClass="form-control ftextstyle mart10" runat="server" ></asp:TextBox>--%>
                                                <a href="#">Change PIN</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="space30"></div>
                                <hr>
                                <p class="cancelaccount">Cancel my edoe Account</p>
                                <asp:Button ID="btnSubmittoProfile" CssClass="btn btn-primary text-center signup-fb" runat="server" Text="Update Profile" OnClick="btnSubmittoProfile_Click" />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----- End Text Area -------------->

        <!----- Footer ---------->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h3>Learn more </h3>
                        <ul>
                            <li><a href="#">How it works </a></li>
                            <li><a href="#">Business </a></li>
                            <li><a href="#">Security </a></li>
                            <li><a href="ContactSupport.aspx">Contact Us </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Company </h3>
                        <ul>
                            <li><a href="#">Our Team </a></li>
                            <li><a href="#">Jobs </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Community </h3>
                        <ul>
                            <li><a href="#">Blog </a></li>
                            <li><a href="#">Help Center </a></li>
                            <li><a href="#">Developer </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <h3>Terms </h3>
                        <ul>
                            <li><a href="#">Legal </a></li>
                            <li><a href="#">Privacy </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/appstore.png" class="img-responsive" alt="appstore" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="appdownload">
                            <a href="#">
                                <img src="images/googleplay.png" class="img-responsive" alt="googleplay" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!----- End Footer ------->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slider.js"></script>
        <script src="js/navstick.js"></script>

        <script type="text/javascript">
            function CanclebTNcLICK()
            {
                $('#imgProfileSet').attr('src', $('#UserProfileDp').attr('src'));
            }

            function ShowPreview(input) {
                if (input.files && input.files[0]) {
                    var ImageDir = new FileReader();
                    ImageDir.onload = function (e) {
                        $('#imgProfileSet').attr('src', e.target.result);
                        //$('#imgProfileSet').height(200);
                        $('#btnCancelUDp').css("display", 'block');
                        $('#btnUserDPUpload').css("display", 'block');
                    }
                    ImageDir.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </form>
</body>
</html>
